ymaps.ready(async () => {
    const map = new ymaps.Map('map', {
        center: [55.751574, 37.573856],
        zoom: 11
    });
    const scootersCluster = new ymaps.Clusterer();
    const IconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div style="background: #fff;">$[properties.iconContent]</div>'
    );
    const res = await fetch('./data.json');
    const data = await res.json();

    if (Array.isArray(data)) {
        data.forEach((x) => {
            scootersCluster.add(
                new ymaps.Placemark(x.point, {
                    iconContent: x.code || ''
                }, {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: 'icons/scooter.svg',
                    iconImageSize: [48, 48],
                    iconImageOffset: [-24, -48],
                    iconContentSize: [48, 10],
                    iconContentOffset: [0, -10],
                    iconContentPadding: [2, 2],
                    iconContentLayout: IconContentLayout
                })
            )
        });

        map.geoObjects.add(scootersCluster);
    }
});
